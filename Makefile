all: erl.mk

erl.mk:
	wget -nv -O $@ 'https://raw.github.com/fenollp/erl-mk/master/erl.mk' || rm $@

dep_ring = https://bitbucket.org/fenollp/ring master

include erl.mk

# Your targets after this line.

test: eunit dialyzer

dialyzer:
	[[ -d .eunit ]] && dialyzer -q -r .eunit -Wunmatched_returns -Werror_handling -Wrace_conditions \
                || dialyzer -q -r  ebin  -Wunmatched_returns -Werror_handling -Wrace_conditions

start: all
	erl -pa ebin deps/*/ebin -boot start_sasl -s brocessing

distclean: clean clean-docs
	$(if $(wildcard deps/ ), rm -rf deps/)
	$(if $(wildcard logs/ ), rm -rf logs/)
	$(if $(wildcard erl.mk), rm erl.mk   )
.PHONY: distclean

debug: all
	erl -pa ebin/ -pa deps/*/ebin/
