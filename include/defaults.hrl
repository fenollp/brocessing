%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-

%% Defaults


%% Standard pulse period
-define(DEFAULT_PERIOD, 2000).


%% Registered name of the gen_server
-define(SERVER, brocessing).


%% gen_server's State.

-record(s, {nodes, %% Ring of active nodes.
            pos, %% Position of node picked lastly. %TODO
            names %% Tree of {Name, record n}.
        }).
%% #s.names contains a name associated to a #n.
-record(n, {module, %% Name of module implementing interface.
            pulsar, %% Pid | atom to n's pulsar.
            tasks, %% Queue of tasks.
            previous, %% Task | empty_task: previously brocessed task.
            workers %% Tree of {Pid,Task} of working processes.
        }).

%% End of File
