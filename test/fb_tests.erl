%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(fb_tests).

%% fb_tests: tests for module fb.

-include_lib("eunit/include/eunit.hrl").


%% API tests.

tasks_test() ->
	StartingNum = <<"156515_487705167951161_103556428">>,
	Expected1 = [[[<<"156515_487705167951161_103556428">>,
				   <<"156515_487705167951161_103556429">>,
				   <<"156515_487705167951161_103556430">>,
				   <<"156515_487705167951161_103556431">>,
				   <<"156515_487705167951161_103556432">>,
				   <<"156515_487705167951161_103556433">>]],
				 [[<<"156515_487705167951161_103556434">>,
				   <<"156515_487705167951161_103556435">>,
				   <<"156515_487705167951161_103556436">>,
				   <<"156515_487705167951161_103556437">>,
				   <<"156515_487705167951161_103556438">>,
				   <<"156515_487705167951161_103556439">>]]],
	Expected1 = fb:tasks(2, 6, StartingNum),
	Expected2 = [[[<<"156515_487705167951161_103556428">>]],
				 [[<<"156515_487705167951161_103556429">>]]],
	Expected2 = fb:tasks(2, 1, StartingNum),
	[] = fb:tasks(0, 10, StartingNum),
	[] = fb:tasks(10, 0, StartingNum).


split_times_test() ->
	[[[1,2]], [[3,4]], [[5,6]]] = fb:split_times(3, 2, lists:seq(1,6)),
	[[[1,2,3]], [[4,5,6]]] = fb:split_times(2, 3, lists:seq(1,6)).


id2int_test() ->
	Expected = 156515487705167951161103556428,
	Expected = fb:id2int(<<"156515_487705167951161_103556428">>).


int2id_test() ->
	Int = 156515487705167951161103556428,
	Id = <<"156515_487705167951161_103556428">>,
	Id = fb:int2id(Int),
	<<"156515_487705167951161_103556430">> = fb:int2id(Int + 2),
	<<"156515_487705167951162_103556428">> = fb:int2id(Int + round(1.0e9)),
	ThirtyTwo = 10000*10000 * 10000*10000 * 10000*10000 * 10000*10000,
	{out_of_interval, _List} = fb:int2id(Int + ThirtyTwo).


is_inside_test() ->
	Number = <<"156515_487705167951161_103556428">>,
	Begin = fb:add(-2, Number),
	End = Number,
	true = fb:is_inside(Number, Begin, End),
	false = fb:is_inside(Number, fb:add(1,Number), fb:add(2,Number)).


add_test() ->
	Number = <<"156515_487705167951161_103556428">>,
	Number = fb:add(0, Number),
	<<"156515_487705167951161_103556430">> = fb:add(2, Number),
	<<"156515_487705167951162_103556428">> = fb:add(round(1.0e9), Number),
	TwentyNine = 100000000000000000000000000000, %% round(1.0e23) ≠ 10²³
	<<"256515_487705167951161_103556428">> = fb:add(TwentyNine, Number),
	ThirtyTwo = TwentyNine * 1000,
	{out_of_interval, _List} = fb:add(ThirtyTwo, Number).


%% Internals

%% End of Module.
