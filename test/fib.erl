%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(fib).

%% fib: implements a slow version of Fibonacci's function.

-ifdef(TEST).
- include_lib("eunit/include/eunit.hrl").
-endif.

-export([fib/1]).

%% API

fib(0) -> 0;
fib(1) -> 1;
fib(N) when N > 1 ->
    fib(N - 1) + fib(N - 2).

%% Tests

-ifdef(TEST).
fib_test() ->
	0 = fib(0),
	1 = fib(1),
	1 = fib(2),
	2 = fib(3),
	3 = fib(4),
	5 = fib(5),
	8 = fib(6).
-endif.

%% Internals

%% End of Module.
