%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(stateless_tests).

%% stateless_tests: tests for module stateless.

-include_lib("eunit/include/eunit.hrl").

%% API tests.

brocess_test() ->
    [{ok,5}] = stateless:brocess([{fib,fib,[5]}]),
    [{ok, _Elapsed, 5}] = stateless:brocess([{fib,fib,[5]}], [{timed,true}]).


everywhere_test() ->
    ok = stateless:everywhere([{fib,fib,[5]}]).


brocess_times_test() ->
    Expected = lists:duplicate(9, {ok, 5}),
    Expected = stateless:brocess([{fib,fib,[5]}], [{times, 9}]).


map_test() ->
    Expected = lists:duplicate(3, {ok, 1}),
    Expected = stateless:map(fib,fib, [[1],[1],[1]]).


%% Internals

%% End of Module.
