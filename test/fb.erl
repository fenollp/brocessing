%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(fb).
-behaviour(brocessing_task_server).

%% fb: utilities to find Facebook images using brocessing.

%% Callbacks for brocessing.
-export([init/0]).
-export([on_connect/1]).
-export([make_task/1]).
-export([locally/2]).
-export([terminate/1]).

%% Actually called from outside.
-export([fb/1]).
-export([is_inside/3]).
-export([tasks/3]).

-ifdef(TEST).
- export([id2int/1]).
- export([int2id/1]).
- export([add/2]).
- export([split_times/3]).
-endif.

%% Needed by Internals.
-include("test/fb_user_agents.hrl").

%% Callbacks

%% Called on add(Name, Module).
%% If return is ≠ ok, it is shown and the whole thing stops.
init() ->
	%% One could create Mnesia tables, I suppose.
	io:format("Work on ~p initiated!\n", [?MODULE]),
	_ = application:start(inets),
	ok.

%% Called when a new bro comes up. (Discovered by a call to connect/1
%%   or to refresh_nodes_list/0).
%% It will return {Node, [{Name, ThisFunsReturn}] | {error, What}}.
on_connect(Node) ->
	R = brocessing:brocess([{application,start,[inets]}], [{nodes,[Node]}]),
	io:format("Hi ~p!\n", [Node]),
	R.

%% Called to fill brocessing's work queue.
%% This has to return like {map, Relative Args} | empty_task.
make_task(empty_task) -> %% Beginning
	%% Some state would help build a different task at each call
	Batches = tasks(2, 4, <<"156515_487705167951161_103556426">>),
    {map, ?MODULE,fb, Batches, [{on_each,node}]};
make_task({map, M,F, ArgsL, Options}=_PreviousTask) -> %% Composition
	%% _PreviousTask = the previous return of this function.
	[L] = lists:last(ArgsL),
	LastFbId = lists:last(L),
	Batches = tasks(2, 4, add(1, LastFbId)),
	{map, M,F, Batches, Options};
make_task(_) -> %% End
	empty_task.

%% Called at the return of a bro call. Only processed on local machine.
%% PreviousTask now represents the one task just brocesssed.
%% Returns ok.
locally(_PreviousTask, [{ok, L}|Rest]) ->
	%% Surely do some Mnesia work here. (And use is_inside/3).
	io:format("Returned ~p\n", [L]),%%
	_ = [io:format("Worked: ~p\n", [X]) || X <- L, is_list(X)],
	locally(_PreviousTask, Rest);
locally(_PreviousTask, []) -> ok;
locally(_PreviousTask, ItsReturn) ->
	io:format("Returned ~p\n", [ItsReturn]).%%

%% Called on remove/1.
%% Regarding return value: same policy as init/0.
terminate(ListOfTasksLeft) ->
	%% Should stop what it has started on init/0 and on_connect/1,
	%%   but you wouldn't stop inets if it's used by another work…
	io:format("Work on ~p terminated!\n", [?MODULE]),
	io:format("Those tasks have not been processed yet: ~p\n", [ListOfTasksLeft]),
	ok.


%% Internals

fb(Numbers) ->
	Urls = [url(Number) || Number <- Numbers],
	hit(Urls).


url(<<X:(6*8)/bits, $_, Y:(15*8)/bits, $_, Z:(9*8)/bits>>) ->
	"http://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-ash3/" ++
	binary_to_list(X) ++
	"_" ++
	binary_to_list(Y) ++
	"_" ++
	binary_to_list(Z) ++
	"_q.jpg".


tasks(_, 0, _) -> [];
tasks(MapArgsCount, TasksPerBatch, StartingNumber) ->
	Tasks = [add(N, StartingNumber)
	|| N <- lists:seq(0, MapArgsCount * TasksPerBatch - 1)],
	split_times(MapArgsCount, TasksPerBatch, Tasks).


id2int(<<X:(6*8)/bits, $_, Y:(15*8)/bits, $_, Z:(9*8)/bits>>) ->
	list_to_integer(lists:append([
		binary_to_list(X),
		binary_to_list(Y),
		binary_to_list(Z)
	])).


int2id(Int) when Int > 0 ->
	case integer_to_list(Int) of
		TooFar when length(TooFar) =/= 6 + 15 + 9 ->
			{out_of_interval, TooFar}
		; XYZ ->
			{X, Rest} = lists:split(6, XYZ),
			{Y, Z} = lists:split(15, Rest),
			BX = list_to_binary(X),
			BY = list_to_binary(Y),
			BZ = list_to_binary(Z),
			<<BX/bits, $_, BY/bits, $_, BZ/bits>>
	end.


is_inside(FbId, Begin, End) ->
	F = id2int(FbId),
	id2int(Begin) =< F andalso F =< id2int(End).


%% FbId ~= \d{6}_\d{15}_\d{9} = X_Y_Z
add(N, FbId) when is_integer(N) ->
	int2id(id2int(FbId) + N).

%% [<<"156515_487705167951161_103556428">>] %% This one works
hit(Urls) ->
	Request = fun(Url) ->
		httpc:request(
			head,
			{Url, [{"User-Agent", random_user_agent()}]},
			[],
			[]
		)
	end,
	[case Request(Url) of
		{ok, {{_HttpVersion,200,_HttpMsg}, _Headers, _EmptyList}} ->
			Url;
		{ok, {{_HttpVersion,HttpCode,_HttpMsg}, _Headers, _EmptyList}} ->
			HttpCode
		; Error -> {ko, Error}
	end || Url <- Urls].


%% Lower-level internals

%% List's length should be divisible by SubListLength.
%% Builds a list of [SubList]s to later give to brocessing:map/3,4.
split_times(0, _, List) -> List;
split_times(NbIter, SubListLength, List) ->
	{SubList, Rest} = lists:split(SubListLength, List),
	[[SubList] | split_times(NbIter - 1, SubListLength, Rest)].

random_user_agent() ->
	lists:nth(
		random_int(1, length(?USER_AGENTS)),
		?USER_AGENTS).

random_int(Bottom, Top) ->
	case erlang:trunc(random:uniform() * 10) of
		S when Bottom =< S, S =< Top -> S
		; __________________________ -> random_int(Bottom, Top)
	end.

%% End of Module.
