%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(stateful_tests).

%% stateful_tests: tests for module stateful.

-include_lib("eunit/include/eunit.hrl").

-include("include/defaults.hrl").

%% API tests.

start_link_test() ->
    {ok, _Pid} = stateful:start_link(),
    ok = stateful:terminate().

add_test () ->
    {setup, fun () -> stateful:start_link () end,
            fun () -> stateful:terminate ()  end,
    fun () -> {inorder, [
        ?_assertEqual ({ok, name_added}, stateful:add (broforce, fb)),
        ?_assertEqual ({error, name_taken}, stateful:add (broforce, fb))
    ]} end}.

remove_test () ->
    {setup, fun () -> stateful:start_link () end,
            fun () -> stateful:terminate ()  end,
    fun () -> {inorder, [
        ?_assertEqual ({ok, name_added}, stateful:add (broforce, fb)),
        ?_assertEqual ({ok, name_removed}, stateful:remove (broforce)),
        ?_assertEqual ({error, unknown_name}, stateful:remove (broforce))
    ]} end}.

pulsate_test () ->
    {setup, fun () -> stateful:start_link () end,
            fun () -> stateful:terminate ()  end,
    fun () -> {inorder, [
        ?_assertEqual ({ok, name_added}, stateful:add (broforce, fb)),
        ?_assertEqual ({ok, pulsar_started, 100}, stateful:pulsate (broforce, 100)),
        ?_assertEqual ({ok, pulsar_started, 2000}, stateful:pulsate (broforce)),
        ?_assertEqual ({ok, name_removed}, stateful:remove (broforce))
    ]} end}.

pause_test () ->
    {setup, fun () -> stateful:start_link () end,
            fun () -> stateful:terminate ()  end,
    fun () -> {inorder, [
        ?_assertEqual ({ok, name_added}, stateful:add (broforce, fb)),
        ?_assertMatch ({ok, pulsar_started, _Period}, stateful:pulsate (broforce)),
        ?_assertEqual ({ok, pulsar_paused}, stateful:pause (broforce)),
        ?_assertEqual ({ok, pulsar_paused}, stateful:pause (broforce)),
        ?_assertEqual ({ok, name_removed}, stateful:remove (broforce)),
        ?_assertEqual ({error, unknown_name}, stateful:pause (broforce))
    ]} end}.

connect_test () ->
    {setup, fun () -> stateful:start_link () end,
            fun () -> stateful:terminate ()  end,
    fun () -> {inorder, [
        ?_assertEqual ([], stateful:connect ([])),
        ?_assertEqual ([{nonode@nohost,{error,ignored}}], stateful:connect ([node()]))
    ]} end}.


%% Behavior & State tests

thorough_test () ->
    Name = broforce,
    Module = fb,
    {setup, fun () -> stateful:start_link () end,
            fun () -> stateful:terminate ()  end,
    fun () -> {inorder, [
        ?_assertMatch ({s,{},1,{0,nil}}, stateful:dump ()),
        ?_assertEqual ({ok, name_added}, stateful:add (Name, Module)),
        ?_assertMatch ({s,{},1,{1,{Name,{n,Module,none,{[],[]},empty_task,{0,nil}},nil,nil}}},
                       stateful:dump ()),
        ?_assertEqual ({ok, pulsar_started, 200}, stateful:pulsate (Name, 200)),
        ?_assertMatch ({s,{},1,{1,{Name,{n,Module,_Pulsar,{[],[]},empty_task,{0,nil}},nil,nil}}},
                       stateful:dump ()),
        %timer:sleep (200),
        ?_assertEqual ({ok, pulsar_paused}, stateful:pause (Name)),
        ?_assertEqual ({ok, name_removed}, stateful:remove (Name)),
        ?_assertMatch ({s,{},1,{0,nil}}, stateful:dump ()),
        ?_assertEqual ({ok, name_added}, ?SERVER:add (broforce, fb))
    ]} end}.

%% Internals

%% End of Module.
