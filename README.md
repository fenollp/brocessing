#brocessing • [Bitbucket](https://bitbucket.org/fenollp/brocessing)




##	Overview

A full-featured Erlang library and releasable-application implementing
a general purpose MapReduce that maps around multiple CPU cores and then network nodes.  
I felt the need for this beacuse OTP doesn't provide **a parallel map that
computes different functions over nodes**. Though `rpc:parallel_eval/1` and
`rpc:pmap/3` are good candidates, I needed a mix of both.
Note the spec of `rpc:parallel_eval/1`. `brocessing:brocess/1` does the same thing,
except it handles the sending of code and catching of remote exceptions.

I originally thought it could cross-compile C code concurrently, but more uses can arise:
large-scale scientific modelling, human-sized neural networks, maintenance of
big databases, parallelisation of highly redondant tasks…

## Options

First value is default

* `{on_each, core | node}` split FuncCalls on each node or core then node.
* `{results, with | without}` whether to return the results.
* `{times, N > 0}` duplicates FuncCalls N times.
* `{timed, false | true}` results tuple contains execution time of FuncCall.
* `{verbosity, off | on}` writes occasionally on stdout.
* `{nodes, [node()|nodes()] | Nodes}` sets wich nodes to computes on.
* `{send_code, yes | no}` whether to send modules' code to remote nodes.

You will find the Release version on branch **rel**.

## How It Works

See `src/brocessing_task_server.erl` for thorough detail of the pool.

## Requirements

Erlang R15B in order to support `-callback` attributes.  
**Please, static-check your own modules with Dialyzer in order to be sure
of their conformity to the `brocessing_task_server` behaviour.** As otherwise
the whole application will crash.

### Stateful gen_server application

`src/stateful.erl` manages a pool of nodes and feeds them work each time a pulsar,
a standalone process, says so.  
A backend module has to export certain callbacks in order to use the **brocessing
application**. It must provide the way to build the next task, how to process it,
and at which pace. The app takes care of were to process tasks and how to handle
the nodes.  
Just feed it kittens.

    :::erlang
    1> application:start(brocessing).
    ok
    2> brocessing:connect(['fries@node2.org']).
    [{'fries@node2.org',[]}]
    3> brocessing:add(broforce, fb).
    Work on fb initiated!
    {ok, name_added}
    4> brocessing:pulsate(broforce, 200).
    {ok, pulsar_started, 200}
    Returned [404,404,
              "http://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-ash3/156515_487705167951161_103556428_q.jpg",
              404]
    Worked: "http://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-ash3/156515_487705167951161_103556428_q.jpg"
    Returned [404,404,404,404]
    Returned [404,404,404,404]
    ...
    5> brocessing:pause(broforce).
    {ok, pulsar_paused}
    6> brocessing:remove(broforce).
    {ok, name_removed}
    7> application:stop(brocessing).
    ok

### Stateless module application

This part englobes functionalities developed in `src/stateless.erl`. Those are
naturally also available from the application **library brocessing**.

With a node -name'd fries@node2.org and the same
[cookie](http://stackoverflow.com/questions/451583/erlang-disallowed-nodes-maybe-cookie-question) on both machines:

    :::bash
    cd brocessing.git/
    make all check
    erl -name ketchup -pz ebin/ deps/*/ebin .eunit/

Now you're in an Erlang shell:

    :::erlang
    (ketchup@node1.org)1> net_kernel:connect_node('fries@node2.org').
    true
    (ketchup@node1.org)2> [node()|nodes()].
    ['ketchup@node1.org','fries@node2.org']
    (ketchup@node1.org)3> brocessing:brocess([{fib,fib,[40]}], [{times,10}, {verbosity,on}]).
    ...
    [{ok,102334155},{ok,102334155},...,{ok,102334155}]
    (ketchup@node1.org)4>


`node()` is who we are and `nodes()` is who we are connected to.  
We have just processed `fib:fib(40)` 10 times across node1 and node2!  
  
Say node1 had 4 cores and node2 had 6, then 10 `fib:fib(40)` were processed
on each of those 10 cores at once!
