%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(brocessing_task_server).


%% Behaviour for task-generating modules.
%%
%% <em>on_connect/1</em> do something on the local machine when a node gets
%%   attached. For instance, one can start an application on given remote node.
%%
%% <em>init/0</em> first thing executed. One can initialize Mnesia tables (or
%%   any other database).
%%
%% <em>make_task/1</em> builds a new task given the previously-executed one,
%%   or append it to DB, or query some state from DB and makes the new task.
%%   On first call, given task is the empty_task atom.
%%
%% <em>locally/2</em> store executed task and its result in some way.
%%
%% <em>terminate/0</em> gracefully closes DB, file descriptors, … and end this
%%   work queue.
%%
%% There is no required operation to perform in any of these callbacks other
%% than returning the proper values.
%%
%%
%% The workflow of an implementation of this interface can be described as
%%   follows:
%%       stateful.erl                      a_task_server.erl
%%       ------------                      -----------------
%%         connect/1         ----->          on_connect/1
%%                           <-----            eg. start an application
%%
%%         add/2             ----->          init/0
%%           init            <-----            eg. init Mnesia
%%
%%         pulsate/2         ----->          make_task/1
%%           remote exec.    <-----            from prev. one | local storage
%%           .               ----->          locally/2
%%                                             display | store {result,task}
%%           Loop
%%
%%       [ pause/1           ----->            Pauses Loop ; ¬ pulsate/2 ]
%%
%%         remove/1          ----->          terminate/1
%%                           <-----            eg. save some state
%%
%%         terminate/0       ----->            Nothing gets called!
%%
%%
%% ----------------------------------------------------------------------------

%% API

-type task() :: empty_task
        | {map, M::module(), F::atom(), ArgsL::list(stateless:args()), Options::stateless:options()}
        .


-callback on_connect (Node::node()) -> Response::term().

-callback init () -> ok.

-callback make_task (OldTask::task()) -> NewTask::task().

-callback locally (Task::task(), BrocessedResult::stateless:result()) -> ok.

-callback terminate (QueueLeft::[task()]) -> ok.

%% Internals

%% End of Module.
