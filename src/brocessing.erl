%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(brocessing).

%% brocessing: process tasks across nodes.

%% Library API
-export([brocess/1]).
-export([brocess/2]).
-export([cores/1]).
-export([everywhere/1]).
-export([everywhere/2]).
-export([map/3]).
-export([map/4]).
-export([send_module/2]).

%% Stateful API
-export([start/0]).
-export([start_link/0]).
-export([pulsate/1, pulsate/2]).
-export([add/2]).
-export([pause/1]).
-export([remove/1]).
-export([terminate/0]).
-export([connect/1, refresh_nodes_list/0]).

%% gen_server requirements.
-export([init/1]).
-export([terminate/2]).
-export([code_change/3]).
-export([handle_info/2]).
-export([handle_call/3]).
-export([handle_cast/2]).

%% APIs

%% Stateless API

brocess(FuncCalls) ->
	stateless:brocess(FuncCalls).
brocess(FuncCalls, Options) ->
	stateless:brocess(FuncCalls, Options).

cores(Node) ->
	stateless:cores(Node).

everywhere(FuncCalls) ->
	stateless:everywhere(FuncCalls).
everywhere(FuncCalls, Options) ->
	stateless:everywhere(FuncCalls, Options).

map(Module, Function, ArgsList) ->
	stateless:map(Module, Function, ArgsList).
map(Module, Function, ArgsList, Options) ->
	stateless:map(Module, Function, ArgsList, Options).

send_module(Nodes, Module) ->
	stateless:send_module(Nodes, Module).

%% Stateful API

start() ->
	application:start(brocessing).


pulsate(Name) ->
	stateful:pulsate(Name).
pulsate(Name, Period) ->
	stateful:pulsate(Name, Period).

pause(Name) ->
	stateful:pause(Name).

add(Name, Module) ->
	stateful:add(Name, Module).

remove(Name) ->
	stateful:remove(Name).

terminate() ->
	stateful:terminate().

connect(Nodes) ->
	stateful:connect(Nodes).

refresh_nodes_list() ->
	stateful:refresh_nodes_list().


start_link() ->
	stateful:start_link().

terminate(Reason, State) ->
	stateful:terminate(Reason, State).

code_change(OldVsn, State, Extra) ->
	stateful:code_change(OldVsn, State, Extra).

handle_info(Info, State) ->
	stateful:handle_info(Info, State).

init(Args) ->
	stateful:init(Args).

handle_call(Request, From, State) ->
	stateful:handle_call(Request, From, State).

handle_cast(Request, State) ->
	stateful:handle_cast(Request, State).

%% Internals

%% End of Module.
