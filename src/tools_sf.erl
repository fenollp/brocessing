%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(tools_sf).

%% tools_sf: tools for the gen_server.

-include("include/defaults.hrl").

-export([pulsar/2]).
-export([kill_pulsar/1]).
-export([worker/2]).
-export([kill_workers/1]).
-export([terminate/1]).
-export([connect_to_nodes/4]).

%% API

pulsar(Name, Period) ->
    receive
        die -> ok
    after Period ->
        stateful:pulse(Name),
        pulsar(Name, Period)
    end.

kill_pulsar(Pid) when is_pid(Pid) ->
    Pid ! die;
kill_pulsar(_None) -> ok.


worker(Name, Task) ->
    Worker = self(),
    process_flag(trap_exit, true),
    spawn_link(fun() -> Worker ! brocess(Task) end),
    R = receive Msg -> Msg end,
    stateful:pulse_back(Name, Worker, R).

kill_workers(#n{workers = Workers}) ->
    kill_workers(gb_trees:iterator(Workers));
kill_workers(Iter) ->
    case gb_trees:next(Iter) of
        {Pid, _Task, Iter2} ->
            erlang:exit(Pid, forced_exit),
            kill_workers(Iter2)
        ; _________________ -> ok
    end.


terminate(#s{names = Names}) ->
    terminate(gb_trees:iterator(Names));
terminate(Iter) ->
    case gb_trees:next(Iter) of
        {_Name, N, Iter2} ->
            kill_pulsar(N#n.pulsar),
            kill_workers(N),
            terminate(Iter2)
        ; ______________ -> ok
    end.



brocess({map, M, F, ArgsL, Opts}) ->
    brocessing:map(M, F, ArgsL, Opts).



connect_to_nodes([], _, _, Acc) -> Acc;
connect_to_nodes([Node|Nodes], Ring, Names, Acc) ->
    case ring:has(Node, Ring) of
        true ->
            connect_to_nodes(Nodes, Ring, Names, Acc)
        ; ___ ->
            Connect = (catch net_kernel:connect_node(Node)),
            Rs = on_connects(Node, Names, Connect),
            connect_to_nodes(Nodes, Ring, Names, [{Node,Rs}|Acc])
    end.

%% Internals

on_connects(Node, Names, Conn) ->
    case Conn of
        true -> on_connects_(Node, gb_trees:iterator(Names), [])
        ; __ -> {error, Conn}
    end.
on_connects_(Node, Iter, Acc) ->
    case gb_trees:next(Iter) of
        {Name, N, Iter2} ->
            Module = N#n.module,
            Res = (catch Module:on_connect(Node)),
            on_connects_(Node, Iter2, [{Name, Res}|Acc])
        ; ______________ ->
            Acc
    end.

%% End of Module.
