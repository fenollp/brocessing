%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(stateful).
-behaviour(gen_server).

%% stateful: gen_server handling pool and Brocessing tasks across nodes.

-include("include/defaults.hrl").

%% gen_server requirements.
-export([init/1]).
-export([terminate/2]).
-export([code_change/3]).
-export([handle_info/2]).
-export([handle_call/3]).
-export([handle_cast/2]).

%% API
-export([start_link/0]).
-export([pulsate/1, pulsate/2]).
-export([add/2]).
-export([pause/1]).
-export([remove/1]).
-export([terminate/0]).
-export([connect/1, refresh_nodes_list/0]).

%% “Internals”
-export([pulse/1]).
-export([pulse_back/3]).
%%-ifdef(TEST).
- export([dump/0]).
%%-endif.


%% API

-spec add(Name :: atom(), Module :: module())
    -> {ok, name_added}
    | {error, name_taken}
    | {error, no_module}.
add(Name, Module) ->
    %% Module should suit the ‘brocessing_task_server’ interface.
    %% Return error if Name is taken
    %% Build queue, call Module:init/0, create pulsar
    gen_server:call(?SERVER, {add, Name, Module}).

-spec pulsate(Name :: atom())
    -> {ok, pulsar_started, Period :: pos_integer()}
    | {error, unknown_name}.
pulsate(Name) ->
    pulsate(Name, ?DEFAULT_PERIOD).

-spec pulsate(Name :: atom(), Period :: pos_integer())
    -> {ok, pulsar_started, Period :: pos_integer()}
    | {error, unknown_name}.
pulsate(Name, Period) ->
    %% Create|Replace Name's pulsar
    gen_server:call(?SERVER, {start, Name, Period}).

-spec pause(Name :: atom())
    -> {ok, pulsar_paused}
    | {error, unknown_name}.
pause(Name) ->
    %% Kill|save the associated pulsar
    gen_server:call(?SERVER, {pause, Name}).

-spec remove(Name :: atom())
    -> {ok, name_removed}
    | {error, unknown_name}.
remove(Name) ->
    %% Kill associated pulsar
    %% Kill all associated workers
    %%   You can gracefully shutdown works using pause/1.
    %% Call Module:terminate/1 (possibly save some state)
    %% Empty related data
    gen_server:call(?SERVER, {remove, Name}).

-spec terminate() -> ok.
terminate() ->
    %% Shutdown everything. Prefer remove/1 for gracefulness.
    gen_server:call(?SERVER, terminate).

-spec connect([node()])
    -> [{node(), {error, term()} | {atom(), term()}}].
connect(Nodes) ->
    %% Add to ring if not already there
    %% Call Module:on_connect/1 locally for each named Module
    gen_server:call(?SERVER, {connect, Nodes}).
    % weird: returns [{Node, [] <--- }] on success.
    % Look at hidden nodes (b/c conn to one bro => conn also to its friends)

-spec refresh_nodes_list()
    -> [{node(), {error, term()} | {atom(), term()}}].
refresh_nodes_list() ->
    %% Update inner ring of nodes
    connect(nodes()).

%% “Internals”

dump() ->
    %% Return current state
    gen_server:call(?SERVER, dump).

pulse(Name) ->
    %% Send a signal to the brocessor of queue Name
    gen_server:cast(?SERVER, {pulse, Name}).

pulse_back(Name, Worker, R) ->
    %% Send the result and context back
    gen_server:cast(?SERVER, {pulse_back, Name, Worker, R}).

%% gen_server API

start_link() ->
    {ok, _Pid} = gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

terminate(Reason, State) ->
    tools_sf:terminate(State),
    io:format("\n\tTerminating with ~p.\n",
              [[{reason, Reason}, {state, State}]]).

handle_info(Msg, State) ->
    io:format("Unexpected message ‘~p’.\n", [Msg]),
    {noreply, State}.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

init(_) ->
    S = #s{
        nodes=ring:new(),
        pos=1,
        names=gb_trees:empty()
    },
    {ok, S}.


handle_call({start, Name, Period_}, _From, State) ->
    case gb_trees:lookup(Name, State#s.names) of
        {value, N} ->
        %% Create|Replace pulsar
            tools_sf:kill_pulsar(N#n.pulsar),
            Period = case N#n.pulsar of
                P when is_integer(P), P > 0 -> P
                ; _________________________ -> Period_
            end,
            Pulsar = spawn(tools_sf, pulsar, [Name, Period]),
            NN = N#n{pulsar=Pulsar},
            NNames = gb_trees:enter(Name, NN, State#s.names),
            {reply, {ok, pulsar_started, Period}, State#s{names=NNames}}
        ; ________ ->
            {reply, {error, unknown_name}, State}
    end;

handle_call({add, Name, Module}, _From, State) ->
    case gb_trees:lookup(Name, State#s.names) of
        {value, _} ->
        %% Return error if Name is taken
            {reply, {error, name_taken}, State}
        ; ________ ->
            case is_list(catch Module:module_info()) of
            %% Return error if Module does not exist
                false -> {reply, {error, no_module}, State}
                ; ___ ->
            %% Build queue, call Module:init/0, create pulsar
                ok = (catch Module:init()),
                NN = #n{
                    module=Module,
                    pulsar=none,
                    tasks=queue:new(),
                    previous=empty_task,
                    workers=gb_trees:empty()
                },
                NNames = gb_trees:insert(Name, NN, State#s.names),
                {reply, {ok, name_added}, State#s{names=NNames}}
            end
    end;

handle_call({remove, Name}, _From, State) ->
    case gb_trees:lookup(Name, State#s.names) of
        {value, N} ->
        %% Kill associated pulsar
            tools_sf:kill_pulsar(N#n.pulsar),
        %% Kill all associated workers
            tools_sf:kill_workers(N),
        %% Call Module:terminate/1
            ok = (catch (N#n.module):terminate(queue:to_list(N#n.tasks))),
        %% Empty related data
            NNames = gb_trees:delete(Name, State#s.names),
            {reply, {ok, name_removed}, State#s{names=NNames}}
        ; ________ ->
            {reply, {error, unknown_name}, State}
    end;

handle_call({connect, Nodes}, _From, State) ->
    Ring = State#s.nodes,
    Names = State#s.names,
    %% Connect then call Module:on_connect/1 locally for each named Module
    Res = tools_sf:connect_to_nodes(Nodes, Ring, Names, []),
    %% Add to ring if not already there
    {reply, Res, State#s{nodes=ring:from_list(nodes())}};

handle_call({pause, Name}, _From, State) ->
    case gb_trees:lookup(Name, State#s.names) of
        {value, N} ->
        %% Kill the associated pulsar
            tools_sf:kill_pulsar(N#n.pulsar),
        %% Save pulsar's Period to n.pulsar
            Period = case N#n.pulsar of
                P when is_integer(P), P > 0 -> P
                ; _________________________ -> ?DEFAULT_PERIOD
            end,
            NN = N#n{pulsar=Period},
            NNames = gb_trees:enter(Name, NN, State#s.names),
            {reply, {ok, pulsar_paused}, State#s{names=NNames}}
        ; ________ ->
            {reply, {error, unknown_name}, State}
    end;

handle_call(dump, _From, State) ->
    {reply, State, State};

handle_call(_Msg, _From, State) ->
    {stop, normal, ok, State}.



handle_cast({pulse, Name}, State) ->
    Names = State#s.names,
    case gb_trees:lookup(Name, Names) of
        {value, N} ->
            Tasks = N#n.tasks,
            Module = N#n.module,
            Workers = N#n.workers,
            PreviousTask = N#n.previous,
        %% Make task, enqueue
            NTask = Module:make_task(PreviousTask),
            Queued = queue:in(NTask, Tasks),
        %% Dequeue, brocess, process locally
            NN = case queue:out(Queued) of
                {{value, empty_task}, Dequeued} ->
                    N#n{tasks=Dequeued, previous=empty_task}
                ; {{value, OTask}, Dequeued} ->
                    Worker = spawn(tools_sf, worker, [Name, OTask]),
                    NWorkers = gb_trees:insert(Worker, OTask, Workers),
                    N#n{tasks=Dequeued, previous=OTask, workers=NWorkers}
                ; __________________________ ->
                %% Queue empty: not possible, we just queue:in/2.
                    N#n{tasks=Queued, previous=empty_task}
            end,
            NNames = gb_trees:enter(Name, NN, Names),
            {noreply, State#s{names=NNames}}
        ; ________ ->
            {noreply, State}
    end;

handle_cast({pulse_back, Name, Pid, R}, State) ->
    Names = State#s.names,
    case gb_trees:lookup(Name, Names) of
        {value, N} ->
            Workers = N#n.workers,
            case gb_trees:lookup(Pid, Workers) of
                {value, OTask} ->
                    Module = N#n.module,
                    ok = (catch Module:locally(OTask, R)),
                    NWorkers = gb_trees:delete(Pid, Workers),
                    NN = N#n{workers=NWorkers},
                    NNames = gb_trees:enter(Name, NN, Names),
                    {noreply, State#s{names=NNames}}
               ; _________ ->
               %% Pid not in Workers. Not possible.
                    {noreply, State}
           end
        ; ________ ->
            {noreply, State}
    end;

handle_cast(terminate, State) ->
    {stop, ok, State}.

%% Internals
%% See src/tools_sf.erl

%% End of module
