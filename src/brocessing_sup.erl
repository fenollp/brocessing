%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(brocessing_sup).
-behaviour(supervisor).

-include("include/defaults.hrl").

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
%-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
	RestartStrategy = one_for_one,
	MaxRestarts = 5,
	MaxSecondsBetweenRestarts = 3600,

	RunPolicy = permanent,
	TimeItHasToShutdown = 2000,
	Type = worker,
	ModulesItUses = [stateful, tools_sf, stateless],

	OnlyChild = {?SERVER, {stateful,start_link,[]},
		RunPolicy, TimeItHasToShutdown, Type, ModulesItUses},

	{ok, {{RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},
		 [OnlyChild]
	}}.
