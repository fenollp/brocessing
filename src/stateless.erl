%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(stateless).

%% stateless: module Brocessing tasks across nodes.

-export([brocess/1]).
-export([brocess/2]).
-export([cores/1]).
-export([everywhere/1]).
-export([everywhere/2]).
-export([map/3]).
-export([map/4]).
-export([send_module/2]).


-type nodes() :: [node()].
-type args() :: [any()].
-type funcCall() :: {module(), atom(), args()}.
-type result() :: {ok, term()} | {error, term()}
                | {ok, non_neg_integer(), term()}
                | {error, non_neg_integer(), term()}.
-type option() :: {on_each, core | node}
                | {results, with | without}
                | {times, pos_integer()}
                | {timed, boolean()}
                | {verbosity, off | on}
                | {nodes, nodes()}
                | {send_code, yes | no}
                .

-type funcCalls() :: [funcCall()].
-type results() :: [result()].
-type options() :: [option()].

-export_type([args/0]).
-export_type([options/0]).
-export_type([result/0]).


%% API

%% @doc brocess(FuncCalls, Options) processes FuncCalls on connected nodes
%% (granularity down to their number of cores)
%% and returns a list of {ok, Result} | {error, Error}.
%% Note: rpc:parallel_eval/1 is brocessing:brocess/1.

-spec brocess(funcCalls()) -> results().
brocess(FuncCalls) ->
    brocess(FuncCalls, []).

-spec brocess(funcCalls(), options()) -> results().
brocess(FuncCalls, Options) ->
    main(FuncCalls, Options).



%% @doc Get Node's number of codes.
-spec cores(node()) -> pos_integer().
cores(Node) ->
    case (catch rpc:call(Node, erlang, system_info, [schedulers_online])) of
        Count when is_integer(Count) -> Count
        ; __________________________ -> 1
    end.



%% @doc everywhere(FuncCalls, Options) computes each FuncCall on every nodes.
%%   If count of nodes < count of FuncCalls, it does something with a modulo.
%% Note: you can pass option {results,with} to mimic rpc:eval_everywhere/3,4
%%                        or {results,without} to mimic rpc:multicall/3,4,5.

-spec everywhere(funcCalls()) -> ok.
everywhere(FuncCalls) ->
    everywhere(FuncCalls, []).

-spec everywhere(funcCalls(), options()) -> ok.
everywhere(FuncCalls, Options) ->
    _ = main(FuncCalls, [{on_each, node}] ++ Options),
    ok.




%% @doc map(Module, Function, ArgsList, Options) calls Module:Function
%%   for each Args in ArgsList.
%% Note: brocessing:map/3,4,5 is a simpler rpc:pmap/3.

-spec map(module(), atom(), [args()]) -> results().
map(Module, Function, ArgsList) ->
    map(Module, Function, ArgsList, []).

-spec map(module(), atom(), [args()], options()) -> results().
map(Module, Function, ArgsList, Options) ->
    Expanded = [{Module, Function, Args} || Args <- ArgsList],
    main(Expanded, Options).


%% @doc send_module(Nodes, Module) sends Module's code to all Nodes.
-spec send_module(nodes(), module()) -> ok.
send_module(Nodes, Module) ->
    {Module, Bin, File} = code:get_object_code(Module),
    {_ResL, _BadNodes} = rpc:multicall(
        Nodes, code, load_binary, [Module, File, Bin]),
    ok.



%% Internal API

%% Basic algorithm.
main(FuncCalls, Options) ->
    Nodes = proplists:get_value(nodes, Options, [node()|nodes()]),
    case proplists:get_value(send_code, Options, yes) of
        no -> ok;
        _ -> send_code([Node || Node <- Nodes, Node =/= node()],
                       FuncCalls, Options)
    end,
    Refs = spawn_regarding_options(Nodes, FuncCalls, Options),
    gather_all(Refs, Options).

%% Verbosity
say(Options, Format) ->
    say(Options, Format, []).
say(Options, Format, Data) ->
    case proplists:get_value(verbosity, Options, off) of
        on -> io:format(Format ++ "~n", Data);
        __ -> ok
    end.

%% Sends needed modules to Nodes.
send_code(Nodes, FuncCalls, Options) ->
    _ = [send_module(Nodes, Module)
    || Module <- lists:usort([M || {M, _F, _A} <- FuncCalls])],
    say(Options, "send_code").

%% Spawns FuncCalls on Nodes wrt Options
spawn_regarding_options(Nodes, FuncCalls, Options) ->
    MFAs = case proplists:get_value(times, Options, 1) of
        N when N > 1 -> lists:append(lists:duplicate(N, FuncCalls))
        ; __________ -> FuncCalls
    end,
    spawn_on_each(Nodes, self(), MFAs, Options).

%% Extends lists:split/2 possibilities: no error on N > length(List).
split_list(N, List) when length(List) < N -> {List, []};
split_list(N, List) -> lists:split(N, List).

%% Launch FuncCalls on Nodes:
%%   • N for each Node, N being Node's number of schedulers (≈ cores).
%%   • else one for each Node.
spawn_on_each(Nodes, Master, MFAs, Options) ->
    say(Options, "spawn_on_each"),
    lists:append(
        spawn_on_each(0, ring:from_list(Nodes), Master, MFAs, Options)).
spawn_on_each(_, _, _, [], _) -> [];
spawn_on_each(P, Nodes, Master, FuncCalls, Options) ->
    Pos = P + 1,
    Node = ring:nth(Pos, Nodes),
    {ToSpawn, Rest} = case proplists:get_value(on_each, Options, core) of
        node -> {[hd(FuncCalls)], tl(FuncCalls)}
        ; __ -> split_list(cores(Node), FuncCalls)
    end,
    [spawn_list(Node, Master, ToSpawn, Options)
    |spawn_on_each(Pos, Nodes, Master, Rest, Options)].

%% Handles ‘timed’ option, along with gather_all/2.
spawn_finally(Node, {M, F, A}, Options, Master, Name, Method) ->
    Ref = erlang:make_ref(),
    {Mod, Text} = case proplists:get_value(timed, Options, false) of
        true -> {fun(X) -> timer:tc(fun() -> X end) end, "timed " ++ Name}
        ; __ -> {fun(X) ->                   X      end,             Name}
    end,
    spawn(fun() -> Master ! {Ref, Mod(catch Method(Node, M, F, A))} end),
    say(Options, "spawned ~s '~p' on ~p [~p]", [Text, {M,F,A}, Node, Ref]),
    Ref.

%% Launch FuncCalls on Node, accumulating a Ref for each launch.
spawn_list(Node, Master, FuncCalls, Options) ->
    [case proplists:get_value(results, Options, with) of
        without ->
            spawn_finally(Node, MFA, Options, Master, "rpc:cast/4",
                fun rpc:cast/4)
        ; _____ ->
            spawn_finally(Node, MFA, Options, Master, "rpc:call/4",
                fun rpc:call/4)
    end || MFA <- FuncCalls].

%% Receive previous Refs and accumulate the results or errors.
gather_all(Refs, Opts) ->
    say(Opts, "gather_all"),
    %% ‘badrpc’ catches 'EXIT', 'DOWN' and dead nodes.
    [case proplists:get_value(timed, Opts, false) of
        true ->
            receive
                {Ref, {T, {badrpc, Err}}} -> gather_error(Opts, Ref, Err, T);
                {Ref, {T,           Res}} -> gather_result(Opts, Ref, Res, T)
            end
        ; __ ->
            receive
                {Ref, {badrpc, Error}} -> gather_error(Opts, Ref, Error);
                {Ref,          Result} -> gather_result(Opts, Ref, Result)
            end
    end || Ref <- Refs].

%% Gather utils for Results.
gather_result(Options, Ref, Result) ->
    say(Options, "received res(~p) from [~p]", [Result, Ref]),
    {ok, Result}.
gather_result(Options, Ref, Result, Elapsed) ->
    say(Options, "received res(~p) from [~p] (took ~p)~n",
        [Result, Ref, Elapsed]),
    {ok, Elapsed, Result}.

%% Gather utils for Errors.
gather_error(Options, Ref, Error) ->
    say(Options, "received err(~p) from [~p]~n", [Error, Ref]),
    {error, Error}.
gather_error(Options, Ref, Error, Elapsed) ->
    say(Options, "received err(~p) from [~p] (took ~p)~n",
        [Error, Ref, Elapsed]),
    {error, Elapsed, Error}.


%% End of Module
